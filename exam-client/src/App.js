import React, {Component, Fragment} from 'react';

import {logoutUser} from "./store/actions/userActions";
import {Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {Container} from "reactstrap";
import {NotificationContainer} from "react-notifications";

import CarsList from "./containers/CarsList/CarsList";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import NewCar from "./containers/NewCar/NewCar";
import CarInfo from "./containers/CarInfo/CarInfo";

import './App.css';



class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <NotificationContainer/>
                    <Toolbar
                        user={this.props.user}
                        logout={this.props.logoutUser}
                    />
                </header>
                <Container style={{marginTop: '20px'}}>
                    <Switch>
                        <Route path="/" exact component={CarsList}/>
                        <Route path="/register" exact component={Register}/>
                        <Route path="/login" exact component={Login}/>
                        <Route path="/add-car" exact component={NewCar} />
                        <Route path="/cars/:id" exact component={CarInfo}/>
                        <Route path="/category/:categoryId" exact component={CarsList} />
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
