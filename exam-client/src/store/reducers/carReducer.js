import {FETCH_CAR_SUCCESS, FETCH_CARS_SUCCESS, FETCH_CATEGORY_CAR} from "../actions/carActions";

const initialState = {
    cars: [],
    car: null,
};

const carsReducers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CARS_SUCCESS:
            return {...state, cars: action.cars};
        case FETCH_CAR_SUCCESS:
            return {...state, car: action.car};
        default:
            return state;
    }
};

export default carsReducers;
