import {push} from 'connected-react-router';
import axios from '../../axiosBase';
import {NotificationManager} from "react-notifications";

export const FETCH_CARS_SUCCESS = 'FETCH_CARS_SUCCESS';
export const FETCH_CAR_SUCCESS = 'FETCH_CAR_SUCCESS';
export const CREATE_CAR_SUCCESS = 'CREATE_CAR_SUCCESS';

export const fetchCarsSuccess = cars => ({type: FETCH_CARS_SUCCESS, cars});
export const fetchCarSuccess = car => ({type: FETCH_CAR_SUCCESS, car});
export const createProductSuccess = () => ({type: CREATE_CAR_SUCCESS});

export const fetchCars = () => {
    return dispatch => {
        return axios.get('/cars').then(
            response => dispatch(fetchCarsSuccess(response.data))
        );
    };
};

export const fetchCar = carId => {
    return (dispatch) => {
        return axios.get('/cars/' + carId).then(response => {
            dispatch(fetchCarSuccess(response.data));
        })
    }
};

export const fetchCarByCategory = categoryId => {
    return dispatch => {
        axios.get('/cars?category=' + categoryId).then(response => {
            dispatch(fetchCarsSuccess(response.data))
        })
    }
};

export const createCar = carsData => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        return axios.post('/cars', carsData, {headers: {'Authorization': token}}).then(
            () => {
                NotificationManager.success('Объявление создано');
                dispatch(createProductSuccess());
                dispatch(push('/'));
            }
        ).catch(() => {
            NotificationManager.error('Поля заполнены не корректно');
        });
    };
};

export const deleteCar = carId => {
    return (dispatch) => {
        return axios.delete('/cars/' + carId).then(() => {
            NotificationManager.success('Объявление удалено');
            dispatch(fetchCars());
            dispatch(push('/'));
        })
    }
};
