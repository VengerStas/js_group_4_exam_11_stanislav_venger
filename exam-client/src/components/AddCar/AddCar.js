import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";

class AddCar extends Component {
    state = {
        title: '',
        description: '',
        category: '',
        price: '',
        image: '',
        
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Название"
                    type="text"
                    value={this.state.title}
                    onChange={this.inputChangeHandler}
                    required={true}
                />

                <FormElement
                    propertyName="description"
                    title="Описание"
                    type="textarea"
                    value={this.state.description}
                    onChange={this.inputChangeHandler}
                    required={true}
                />

                <FormGroup row>
                    <Label sm={2} for="category">Категория</Label>
                    <Col sm={10}>
                        <Input
                            type="select" required
                            name="category" id="category"
                            value={this.state.category}
                            onChange={this.inputChangeHandler}
                        >
                            <option value="" disabled>Выберите категорию...</option>
                            {this.props.categories.map(category => (
                                <option key={category._id} value={category._id}>{category.title}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>

                <FormElement
                    propertyName="price"
                    title="Цена"
                    type="number"
                    value={this.state.price}
                    onChange={this.inputChangeHandler}
                    required={true}
                />
                <FormGroup row>
                    <Label sm={2} for="image">Фото авто</Label>
                    <Col sm={10}>
                        <Input
                            type="file"
                            name="image" id="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <Button type="submit">Добавить</Button>
            </Form>
        );
    }
}

export default AddCar;