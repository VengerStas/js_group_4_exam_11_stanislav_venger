import React from 'react';
import {CardImg} from "reactstrap";
import {apiURL} from "../../constants";

const CarsThumbnail = props => {
    if (props.image !== "null") {
        let image = apiURL + '/uploads/' + props.image;
        return <CardImg src={image} className="img-thumbnail" alt="News Image" />;
    }
    return null;
};

export default CarsThumbnail;