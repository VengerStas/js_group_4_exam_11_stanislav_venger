import React, {Component} from 'react';
import {createCar} from "../../store/actions/carActions";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import {Container} from "reactstrap";
import AddCar from "../../components/AddCar/AddCar";
import {fetchCategories} from "../../store/actions/categoryActions";

class NewCar extends Component {
    componentDidMount() {
        this.props.getCategories();
    };

    createCar = carsData => {
        this.props.carCreate(carsData);
    };
    render() {
        if (!this.props.user) return <Redirect to="/register" />;
        return (
            <div className="add-car-block">
                <Container>
                    <h4>Добавить новое объявление</h4>
                </Container>
                <AddCar
                    onSubmit={this.createCar}
                    categories={this.props.categories}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    carCreate: carsData => dispatch(createCar(carsData)),
    getCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewCar);