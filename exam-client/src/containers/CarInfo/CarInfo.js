import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteCar, fetchCar} from "../../store/actions/carActions";
import CarsThumbnail from "../../components/CarsThumbnail/CarsThumbnail";
import {Button, Card, CardBody, CardText, CardTitle} from "reactstrap";
import {Redirect} from "react-router-dom";

import './CarInfo.css';

class CarInfo extends Component {
    componentDidMount() {
        this.props.loadCarInfo(this.props.match.params.id);
    };

    deleteHandler = (carId) => {
        this.props.deleteCar(carId);
        this.props.history.push('/');
    };


    render() {
        if (!this.props.car) return <div>Loading</div>;
        if (!this.props.users) return <Redirect to='/login'/>;
        return (
            <div className="car-block-info">
                <Card className="card-item-info">
                    <CarsThumbnail image={this.props.car.image}/>
                    <CardBody>
                        <CardTitle><strong>Название: </strong>{this.props.car.title}</CardTitle>
                        <CardText><strong>Описание: </strong>{this.props.car.description}</CardText>
                        <CardText><strong>Категория: </strong>{this.props.car.category.title}</CardText>
                        <CardText><strong>Price: </strong>{this.props.car.price}$</CardText>
                        <CardText><strong>Имя продавца: </strong>{this.props.car.user.displayname}</CardText>
                        <CardText><strong>Номер телефона: </strong>+{this.props.car.user.phone}</CardText>
                        {
                            (this.props.users._id === this.props.car.user._id) ? <Button onClick={() => this.deleteHandler(this.props.car._id)}>Delete</Button> : null
                        }
                    </CardBody>
                </Card>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    users: state.users.user,
    car: state.cars.car,
});

const mapDispatchToProps = dispatch => ({
    loadCarInfo: (carId) => dispatch(fetchCar(carId)),
    deleteCar: (cardId) => dispatch(deleteCar(cardId))
});

export default connect(mapStateToProps, mapDispatchToProps)(CarInfo);