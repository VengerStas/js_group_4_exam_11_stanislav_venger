import React, {Component} from 'react';
import {Button, Card, CardBody, CardText, CardTitle, Nav, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {connect} from "react-redux";
import {fetchCarByCategory, fetchCars} from "../../store/actions/carActions";
import {fetchCategories} from "../../store/actions/categoryActions";
import CarsThumbnail from "../../components/CarsThumbnail/CarsThumbnail";

import './CarsList.css';



class CarsList extends Component {
    componentDidMount() {
        this.props.getCategories();
        this.props.getCars();

    };

    componentDidUpdate(prevProps, prevState) {
        if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
            this.props.fetchCarByCategory(this.props.match.params.categoryId)
        }
    }

    render() {
        let car = this.props.cars.map(car => {
            return (
                <Card key={car._id} className="car-item">
                    <CarsThumbnail image={car.image}/>
                    <CardBody>
                        <CardTitle>{car.title}</CardTitle>
                        <CardText>{car.price}$</CardText>
                        <Button tag={RouterNavLink}  to={'/cars/' + car._id}>Посмотреть</Button>
                    </CardBody>
                </Card>
            )
        });

        let category = this.props.categories.map(category => {
            return (
                <NavItem key={category._id}>
                    <NavLink tag={RouterNavLink} to={'/category/' + category._id}>{category.title}</NavLink>
                </NavItem>
            )
        });

        return (
            <div className="cars-market">
                <div className="cars-block">
                    <div className="cars-nav-category">
                        <p>Категории:</p>
                        <Nav vertical>
                            {category}
                        </Nav>
                    </div>
                    <div className="cars-list-item">
                        <h4>Авто на продажу</h4>
                        <div className="car-items">
                            {car}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cars: state.cars.cars,
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    getCars: () => dispatch(fetchCars()),
    fetchCarByCategory: (categoryId) => dispatch(fetchCarByCategory(categoryId)),
    getCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(CarsList);