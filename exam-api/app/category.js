const express = require('express');
const Category = require('../models/Category');
const Car = require('../models/Car');
const router = express.Router();

router.get('/', (req, res) => {
    Category.find()
        .then(categories => res.send(categories))
        .catch(() => res.sendStatus(500));
});

module.exports = router;