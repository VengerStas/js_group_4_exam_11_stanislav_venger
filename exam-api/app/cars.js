const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const Car = require('../models/Car');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.category) {
        Car.find({category: req.query.category}).populate('category', '_id title')
            .then(cars => res.send(cars))
            .catch(() => res.sendStatus(500));
    } else {
        Car.find().populate('category', '_id title')
            .then(cars => res.send(cars))
            .catch(() => res.sendStatus(500));
    }
});

router.get('/:id', (req, res) => {
    Car.findById(req.params.id).populate({path: 'user'}).populate({path: 'category'})
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

router.post('/', auth, upload.single('image'), (req, res) => {
    const carData = {...req.body, user: req.user._id};

    if (req.file) {
        carData.image = req.file.filename;
    }

    const car = new Car(carData);

    if (req.body.price < 0) {
        return res.sendStatus(403)
    } else {
        car.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    }

});

router.delete('/:id', (req, res) => {
   Car.findByIdAndDelete(req.params.id)
        .then(result => res.send(result))
       .catch(error => res.status(403).send(error))
});


module.exports = router;
