const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const User = require('./models/User');
const Car = require('./models/Car');
const Category = require('./models/Category');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users = await User.create(
        {username: "Bond777", password: "123", displayname: "James Bond", phone: "77777777777", token: nanoid()},
        {username: "Tom666", password: "123", displayname: "Tom", phone: "666666666", token: nanoid()}
    );

    const [passenger, minivan, jeep, trucks, motorbike] = await Category.create(
        {title: 'Легковые'},
        {title: 'Минивэны'},
        {title: 'Джипы'},
        {title: 'Грузовые'},
        {title: 'Мотоциклы'},
    );

    await Car.create(
        {user: users[0]._id, title: 'Aston Martin', description: 'Не бит, не крашен', price: '20000', image: 'aston.jpg', category: passenger._id,},
        {user: users[1]._id, title: 'Уазик буханка',description: 'УАЗ', price: '6666', image: 'buhanka.jpg', category: minivan._id}
    );

    return connection.close();
};


run().catch(error => {
    console.error('Something wrong happened...', error);
});